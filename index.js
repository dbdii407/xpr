#!/usr/bin/node

"use strict"

const p$input = require(':ptyps/input')
const p$argv = require(':ptyps/argv')
const p$next = require(':ptyps/next')
const p$run = require(':ptyps/run')
const p$fs = require(':ptyps/fs')
const chalk = require('chalk')
const q = require('bluebird')

function exists(...paths) {
  let existance = q.map(paths, path => p$fs.resolve(process.env.PWD, path)).map(path => p$fs.exists(path))

  return new q.Promise(res => q.join.apply(null, [].concat(existance).concat(results => {
    let exists = results.every(result => result == !0)
    res(exists)
  })))
}

function run(command, fn) {
  let echo = chalk.gray.italic(command)

  return new q.Promise(res => {
    let it = p$run(command, {cwd: process.env.PWD})
    console.log(echo)

    it.on('error', buffer => {
      let error = buffer.toString().trim()
      return fn(error)
    })

    it.on('data', buffer => {
      let line = buffer.toString().trim()
      return fn(null, line)
    })
  
    it.on('done', code => {
      fn(null, null, code)
      res()
    })
  })
}

function yarn(command) {
  return new q.Promise((res, rej) => {
    run(`yarn ${command}`, (err, line, code) => {
      if (!err && !line && code === 0)
        return res()

      if (!err && !line && code !== 0)
        return rej(code)

      let message = (err || line).trim()

      if (!['\u001b[1G', '\u001b[2K', '\u001b[2K\u001b[1G'].some(u => u == message))
        return console.log(message)
    })
  })
}

function git(command) {
  return new q.Promise((res, rej) => {
    run(`git ${command}`, (err, line, code) => {
      if (!err && !line && code === 0)
        return res()

      if (!err && !line && code !== 0)
        return rej(code)

      let message = (err || line).trim()
      console.log(message)
    })
  })
}

let version = p$argv.has('version')
let help = p$argv.has('help')
let argv = p$argv.next()

if (help && !argv) {
  let commands = {
    init: 'Create a new project',
    install: 'Installs packages',
    affix: 'Adds files to a git project',
    tag: 'Commits changes to a git project',
    impel: 'Adds files and commits changes to a git project',
    status: 'Shows the changes in a git project',
    push: 'Submit project changes to a git service',
    deps: 'Shows dependencies'
  }

  console.log(`Usage: xpr <command>`)
  console.log('xpr uses yarn and git commands to help make development faster.\n')
  console.log('Commands:')

  return Object.entries(commands)
    .forEach(([command, desc]) => console.log(` ${command.padEnd(20, ' ')} ${desc}`))
}

if (version && !argv)
  return exists('package.json').then(exists => {
    if (!exists)
      return

    let file = p$fs.resolve(__dirname, 'package.json')

    return p$fs.read(file).then(json => console.log(`xpr version: ${json.version}`))
  })

if (!argv)
  return exists('package.json').then(exists => {
    if (!exists)
      return

    let file = p$fs.resolve(process.env.PWD, 'package.json')

    return p$fs.read(file).then(json => {
      let script = p$fs.resolve(process.env.PWD, json.main)
      let flags = []
  
      if (script.endsWith('.mjs'))
        flags = flags.concat('--experimental-modules', '--no-warnings')
  
      let command = [].concat('node').concat(flags).concat(script).join(' ')
      
      return run(command, (err, line, code) => {
        if (!err && !line)
          return

        let message = (err || line).trim()
        console.log(message)
      })
    })
  })

if (argv == 'rename')
  return exists('package.json').then(exists => {
    if (!exists) return

    let file = p$fs.resolve(process.env.PWD, 'package.json')

    return p$fs.read(file).then(json => {
      let line = `${chalk.bold('xpr')} What would you like the new name to be?`
      
      return p$input(`${line} `).then(name => {
        json.name = name

        return p$fs.write(file, json)
      })
    })
  })

if (argv == 'deps')
  return exists('package.json').then(exists => {
    if (!exists)
      return

    let file = p$fs.resolve(process.env.PWD, 'package.json')

    return p$fs.read(file).then(json => {
      if (!json.dependencies)
        return console.log('there are no dependencies for this project')

      console.log(`${chalk.bold(json.name)} dependencies`)

      for (let [dep, altern] of Object.entries(json.dependencies)) {
        let line = ` - ${dep.padEnd(24, ' ')} ${chalk.italic.gray(altern)}`
        console.log(line)
      }
    })
  })

if (argv == 'init')
  return exists('package.json').then(exists => {
    if (exists)
      return

    let json = {}, questions = []

    questions.push({
      type: 'name',
      default: p$fs.basename(process.env.PWD),
      fn: function(response, done) {
        json.name = response || this.default
        done()
      }
    })

    questions.push({
      type: 'license',
      default: 'MIT',
      fn: function(response, done) {
        json.license = response || this.default
        done()
      }
    })

    questions.push({
      type: 'version',
      default: '1.0.0',
      fn: function(response, done) {
        json.version = response || this.default
        done()
      }
    })

    questions.push({
      type: 'type',
      default: 'js',
      fn: function(response, done) {
        if (response && !['js', 'mjs'].includes(response))
          response = null

        json.main = `index.${response || this.default}`
        done()
      }
    })

    questions.push({
      type: 'git',
      default: 'service:group/project',
      fn: function(response, done) {
        if (!response)
          return done()

        let [, service, group, name] = response.match(/(.*):(.*)\/(.*)/)

        if (!service.includes('.'))
          service += '.com'

        json.repository = `git@${service}:${group}/${name}.git`
        done()
      }
    })

    questions = questions.map(question => done => {
      let line = `${chalk.bold('xpr')} ${question.type}`

      if (question.default)
        line += ` (${question.default})`

      p$input(`${line}: `).then(response => question.fn(response, done))
    })

    p$next(questions).then(() => {
      let echo = chalk.gray.italic(`touch package.json`)
      console.log(echo)

      let file = p$fs.resolve(process.env.PWD, 'package.json')
      return p$fs.write(file, json)
    })
    
    .then(() => yarn('install'))

    .then(() => {
      if (!json.repository)
        return

      return new q.Promise(res => run(`git init`, (err, line, code) => {
        if (!err && !line)
          return res(code)
  
        let message = (err || line).trim()
        return console.log(message)
      }))

      .then(code => {
        if (code !== 0)
          return

        return run(`git remote add origin ${json.repository}`, (err, line, code) => {
          if (!err && !line)
            return
    
          let message = (err || line).trim()
          return console.log(message)
        })
      })

      .then(() => {
        let echo = chalk.gray.italic(`touch .gitignore`)
        console.log(echo)

        let ignore = p$fs.resolve(process.env.PWD, '.gitignore')
        return p$fs.write(ignore, `node_modules/*\nyarn.lock\nyarn-error.log`)
      })
    })

    .then(() => {
      let echo = chalk.gray.italic(`touch ${json.main}`)
      console.log(echo)

      let index = p$fs.resolve(process.env.PWD, json.main)
      return p$fs.write(index, `"use strict"\n\n`)
    })
  })

if (argv == 'install')
  return exists('package.json').then(exists => {
    if (!exists)
      return

    let list = p$argv.all().map(project => {
      if (!['/', ':'].every(symbol => project.includes(symbol)))
        return project

      let [, service, group, name] = project.match(/(.*):(.*)\/(.*)/)
      return `:${group}/${name}@${service}:${group}/${name}`
    })

    return yarn(`add ${list.join(' ')}`)
  })

if (argv == 'remove')
  return exists('package.json').then(exists => {
    if (!exists)
      return

    let list = p$argv.all()

    return yarn(`remove ${list.join(' ')}`)
  })

if (argv == 'reset')
  return exists('package.json').then(exists => {
    if (!exists)
      return

    return p$fs.readdir(process.env.PWD).filter(file => {
      if (file.includes('yarn') || file == 'node_modules')
        return !0
    })

    .map(file => p$fs.resolve(process.env.PWD, file))

    .each(file => {
      let echo = chalk.italic.gray(`rm -rf ${file}`)
      console.log(echo)

      return p$fs.remove(file)
    })

    .then(() => yarn('install'))
  })

if (argv == 'affix')
  return exists('package.json', '.git').then(exists => {
    if (!exists)
      return

    let list = p$argv.all(), fixings = [].concat(list.length ? list : '.').join(' ')

    return run(`git add ${fixings}`, (err, line, code) => {
      if (!err && !line)
        return

      let message = (err || line).trim()
      console.log(message)
    })
  })

if (argv == 'tag')
  return exists('package.json', '.git').then(exists => {
    if (!exists)
      return

    let tag = p$argv.all().join(' ')
    
    return run(`git commit -m "${tag}"`, (err, line, code) => {
      if (!err && !line)
        return

      let message = (err || line).trim()
      console.log(message)
    })
  })

if (argv == 'impel')
  return exists('package.json', '.git').then(exists => {
    if (!exists)
      return

    let tag = p$argv.all().join(' ')
    
    return run(`git commit -a -m "${tag}"`, (err, line, code) => {
      if (!err && !line)
        return

      let message = (err || line).trim()
      console.log(message)
    })    
  })

if (argv == 'status')
  return exists('package.json', '.git').then(exists => {
    if (!exists)
      return

    return run(`git status`, (err, line, code) => {
      if (!err && !line)
        return

      let message = (err || line).trim()
      console.log(message)
    })
  })

if (argv == 'push')
  return exists('package.json', '.git').then(exists => {
    if (!exists)
      return

    return new q.Promise(res => {
      let upstream

      run(`git rev-parse --abbrev-ref master@{u}`, (err, line, code) => {
        if (code !== undefined)
          return res(upstream)

        if (line)
          upstream = line
      })
    })

    .then(upstream => upstream ? 'git push' : 'git push -u origin master')

    .then(command => run(command, (err, line, code) => {
      if (!err && !line)
        return

      let message = (err || line).trim()
      console.log(message)
    }))
  })